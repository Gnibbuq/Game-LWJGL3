package fanworld.core.org.lwjglb.game;

import fanworld.core.org.lwjglb.engine.IGameLogic;
import fanworld.core.org.lwjglb.engine.MouseInput;
import fanworld.core.org.lwjglb.engine.Scene;
import fanworld.core.org.lwjglb.engine.SceneLight;
import fanworld.core.org.lwjglb.engine.Window;
import fanworld.core.org.lwjglb.engine.graph.CoreCamera;
import fanworld.core.org.lwjglb.engine.graph.Renderer;
import fanworld.core.org.lwjglb.engine.graph.lights.DirectionalLight;
import fanworld.core.org.lwjglb.engine.graph.weather.Fog;
import fanworld.core.org.lwjglb.engine.items.GameItem;
import fanworld.core.org.lwjglb.engine.items.SkyBox;
import fanworld.core.org.lwjglb.engine.sound.SoundListener;
import fanworld.core.org.lwjglb.engine.sound.SoundManager;
import fanworld.core.org.lwjglb.engine.sound.SoundSource;
import fanworld.game.client.GameData;
import fanworld.game.client.command.CommandManager;
import fanworld.game.client.input.keyboard.KeyBoardManager;
import fanworld.game.client.sounds.SoundUtil;
import fanworld.game.core.Game;
import fanworld.game.world.CraftWorld;
import fanworld.game.world.Location;
import fanworld.game.world.block.CraftBlock;
import fanworld.game.world.block.blockdata.BlockType;
import fanworld.game.world.block.blockdata.PlanksOak;
import fanworld.game.world.block.chunk.Chunk;

import java.util.ArrayList;
import java.util.List;

import org.joml.Vector3f;
import org.joml.Vector4f;
import static org.lwjgl.glfw.GLFW.*;

import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.openal.AL11;

public class DummyGame implements IGameLogic {
	public static DummyGame INSTANCE;
	
    private static final float MOUSE_SENSITIVITY = 0.2f;

    /**
     * 摄像机位置
     */
    private final Vector3f cameraInc;

    /**
     * 渲染器
     */
    private final Renderer renderer;

    /**
     * 声音管理器
     */
    private final SoundManager soundMgr;

    /**
     * 摄像机
     */
    private final CoreCamera coreCamera;

    /**
     * 屏幕
     */
    private Scene scene;

    /**
     * HUD加载器
     */
    private HudLoader hudLoader;

    private float angleInc;

    private float lightAngle;//TODO 光线角度

    private CameraBoxSelectionDetector selectDetector;

    /**
     * 左键是否按下
     */

    private boolean firstTime;

    /**
     * 屏幕是否改变
     */
    private boolean sceneChanged;

    /**
     * 声音枚举
     */
    public enum Sounds {
        FIRE
    };

    private List<GameItem> gameItems;
    
    private Game game;

    public DummyGame() {
    	gameItems=new ArrayList<>();
        renderer = new Renderer();
        hudLoader = new HudLoader();
        soundMgr = new SoundManager();
        coreCamera = new CoreCamera();
        cameraInc = new Vector3f(0.0f, 0.0f, 0.0f);
        angleInc = 0;
        lightAngle = 90;//光线
        INSTANCE=this;
        firstTime = true;
        game=new Game(coreCamera);
    }

    double lastPosX,lastPosY;
    @Override
    public void init(Window window) throws Exception {
        hudLoader.init(window);
        renderer.init(window);

        glfwSetCursorPosCallback(window.getWindowHandle(), new GLFWCursorPosCallback() {
    	    @Override
    	    public void invoke(long windowHandle, double xpos, double ypos) {
    	        //更新摄像机面向方向
    	        coreCamera.moveRotation((float)((ypos-lastPosY)/2) * MOUSE_SENSITIVITY, (float)((xpos-lastPosX)/2) * MOUSE_SENSITIVITY, 0);
    	        sceneChanged = true;
    	        
    	        lastPosX=xpos;//TODO 鼠标移动
    	        lastPosY=ypos;
    	    }
    	});

        scene = new Scene();

        float blockScale = 0.5f;
        float skyBoxScale = 100.0f;
        float extension = 2.0f;

        float startx = extension * (-skyBoxScale + blockScale);
        float startz = extension * (skyBoxScale - blockScale);
        float starty = -1.0f;
        float inc = blockScale * 2;

        float posx = startx;
        float posz = startz;
        float incy = 0.0f;

        selectDetector = new MouseBoxSelectionDetector();

        int height = 500;
        int width = 500	;

        List<Chunk> loadedChunks=new ArrayList<>();
        CraftWorld craftWorld=game.getGameClient().getCraftWorld();
        
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                //TODO SHOULD NOT BE HERE
                incy = craftWorld.getNoise().getY(((int)posx), ((int)posz));//TODO 高度图
              //TODO SHOULD NOT BE HERE
                //incy = rgb / (10 * 255 * 255);
                for(int k=1;k<=3;k++) {
                	float tempY=incy+k;
                	Location nowLoc=new Location(craftWorld,posx, starty + tempY, posz);
                	CraftBlock newBlock=new CraftBlock(nowLoc,BlockType.GRASS);
                	Chunk nowChunk=craftWorld.getChunk(nowLoc);
                	loadedChunks.add(nowChunk);
                	nowChunk.addBlock(newBlock);
                	GameItem newGameItem = newBlock.getGameItem();
                    gameItems.add(newGameItem);
                }
                posx += inc;
            }
            posx = startx;
            posz -= inc;
        }
        for(Chunk chunk:loadedChunks) {
        	chunk.init();
        }
        //TreeUtil.createTree(new Location(craftWorld,-150,10,150));
        
        this.reviewGameItem();
        
        // 形状
        //TODO FIRE
        /**
        int maxParticles = 200;
        Vector3f particleSpeed = new Vector3f(0, 1, 0);
        particleSpeed.mul(2.5f);
        long ttl = 4000;
        long creationPeriodMillis = 300;
        float range = 0.2f;
        float scale = 1.0f;
        Mesh partMesh = OBJLoader.loadMesh("/models/particle.obj", maxParticles);
        Texture particleTexture = new Texture("/textures/particle_anim.png", 4, 4);
        Material partMaterial = new Material(particleTexture, reflectance);
        partMesh.setMaterial(partMaterial);
        Particle particle = new Particle(partMesh, particleSpeed, ttl, 100);//火
        particle.setScale(scale);
        particleEmitter = new FlowParticleEmitter(particle, maxParticles, creationPeriodMillis);
        particleEmitter.setActive(true);
        particleEmitter.setPositionRndRange(range);
        particleEmitter.setSpeedRndRange(range);
        particleEmitter.setAnimRange(10);
        this.scene.setParticleEmitters(new FlowParticleEmitter[]{particleEmitter});//设置形状*/

        // 阴影
        scene.setRenderShadows(true);

        // 雾
        Vector3f fogColour = new Vector3f(0.5f, 0.5f, 0.5f);
        scene.setFog(new Fog(true, fogColour, 0.02f));

        // 设置天空盒
        SkyBox skyBox = new SkyBox("/models/skybox.obj", new Vector4f(0.65f, 0.65f, 0.65f, 1.0f));
        skyBox.setScale(skyBoxScale);
        scene.setSkyBox(skyBox);

        // 初始化光
        setupLights();

        coreCamera.getPosition().x = -150f;
        coreCamera.getPosition().y = 6.5f;//设置位置
        coreCamera.getPosition().z = 150f;
        coreCamera.getRotation().x = 0;
        coreCamera.getRotation().y = -1;

        // 声音系统初始化
        this.soundMgr.init();
        this.soundMgr.setAttenuationModel(AL11.AL_EXPONENT_DISTANCE);
        setupSounds();
        
        //指令系统初始化
        CommandManager.INSTANCE.init();
    }

    /**
     * 初始化声音
     * @throws Exception
     */
	private void setupSounds() throws Exception {
    	
        SoundBuffer buffFire = new SoundBuffer("/sounds/music1.ogg");
        soundMgr.addSoundBuffer(buffFire);
        SoundSource sourceFire = new SoundSource(true, false);
        //Vector3f pos = particleEmitter.getBaseParticle().getPosition();//TODO
        Vector3f pos = this.getCoreCamera().getPosition();
        sourceFire.setPosition(pos);
        sourceFire.setBuffer(buffFire.getBufferId());
        soundMgr.addSoundSource("music1", sourceFire);
        sourceFire.play();

        soundMgr.setListener(new SoundListener(pos));
    }

    private void setupLights() {
        SceneLight sceneLight = new SceneLight();
        scene.setSceneLight(sceneLight);

        // 环境光
        sceneLight.setAmbientLight(new Vector3f(0.3f, 0.3f, 0.3f));
        sceneLight.setSkyBoxLight(new Vector3f(1.0f, 1.0f, 1.0f));

        // 方向光
        float lightIntensity = 1.0f;
        Vector3f lightDirection = new Vector3f(0, 1, 1);
        DirectionalLight directionalLight = new DirectionalLight(new Vector3f(1, 1, 1), lightDirection, lightIntensity);
        sceneLight.setDirectionalLight(directionalLight);
    }

    @Override
    public void input(Window window, MouseInput mouseInput) {
        sceneChanged = false;
        cameraInc.set(0, 0, 0);
        KeyBoardManager.INSTANCE.input(window);
        if(CommandManager.INSTANCE.isUsingCommand())return;
        if (window.isKeyPressed(GLFW_KEY_W)) {
            sceneChanged = true;
            cameraInc.z = -1;
        } else if (window.isKeyPressed(GLFW_KEY_S)) {
            sceneChanged = true;
            cameraInc.z = 1;
        }
        if (window.isKeyPressed(GLFW_KEY_A)) {
            sceneChanged = true;
            cameraInc.x = -1;
        } else if (window.isKeyPressed(GLFW_KEY_D)) {
            sceneChanged = true;
            cameraInc.x = 1;
        }
        if (window.isKeyPressed(GLFW_KEY_LEFT_SHIFT)) {
            sceneChanged = true;
            cameraInc.y = -1;
        } else if (window.isKeyPressed(GLFW_KEY_SPACE)) {
            sceneChanged = true;
            cameraInc.y = 1;
        }
        if (window.isKeyPressed(GLFW_KEY_LEFT)) {
            sceneChanged = true;
            angleInc -= 0.05f;
        } else if (window.isKeyPressed(GLFW_KEY_RIGHT)) {
            sceneChanged = true;
            angleInc += 0.05f;
        } else {
            angleInc = 0;
        }
    }

    @Override
    public void update(float interval, MouseInput mouseInput, Window window) {
    	// 更新摄像机坐标
        coreCamera.movePosition(cameraInc.x * GameData.CAMERA_POS_STEP, cameraInc.y * GameData.CAMERA_POS_STEP, cameraInc.z * GameData.CAMERA_POS_STEP);
        
        if(mouseInput.isLeftButtonPressed()) {
        	DummyGame.INSTANCE.getHudLoader().incCounter();
        }
        
        lightAngle += angleInc;
        if (lightAngle < 0) {
            lightAngle = 0;
        } else if (lightAngle > 180) {
            lightAngle = 180;
        }
        float zValue = (float) Math.cos(Math.toRadians(lightAngle));
        float yValue = (float) Math.sin(Math.toRadians(lightAngle));
        Vector3f lightDirection = this.scene.getSceneLight().getDirectionalLight().getDirection();
        lightDirection.x = 0;
        lightDirection.y = yValue;
        lightDirection.z = zValue;
        lightDirection.normalize();///TODO walking light

        //particleEmitter.update((long) (interval * 1000));//TODO SOUND

        // 更新视角矩阵
        coreCamera.updateViewMatrix();

        //更新声音监听坐标;
        soundMgr.updateListenerPosition(coreCamera);//TODO SOUND

        /**
         * 物品选择
         */
        boolean aux = mouseInput.isLeftButtonPressed();
        
        //TODO undone
        GameItem gameItem=selectDetector.selectGameItem(gameItems, coreCamera);
        if(aux) {
        	if(gameItem!=null) {
        		SoundUtil.playGrassSound(gameItem.getPosition());
        		gameItem.setDisplay(false);
        	}
        }else if(mouseInput.isRightButtonPressed()) {
			PlanksOak planksOak;
			if(gameItem!=null)
			try {
				//System.out.println("test");
				planksOak = new PlanksOak();
				GameItem gameItemTemp = new GameItem(planksOak.getMesh());
                gameItemTemp.setScale(0.5f);
                Vector3f position=gameItem.getPosition();
                gameItemTemp.setPosition(position.x, position.y+1 , position.z);
                scene.addGameItem(gameItemTemp);
                gameItems.add(gameItemTemp);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    }
    
    public void addGameItem(GameItem gameItem) {
    	gameItems.add(gameItem);
    }
    
    public void reviewGameItem() {
        scene.setGameItems(gameItems);
    }
    
    public CoreCamera getCoreCamera() {
    	return this.coreCamera;
    }
    
    public Game getGame() {
    	return this.game;
    }

    @Override
    public void render(Window window) {
        if (firstTime) {
            sceneChanged = true;
            firstTime = false;
        }
        renderer.render(window, coreCamera, scene, sceneChanged);
        hudLoader.render(window);
    }
    
    @Override
    public void cleanup() {
        renderer.cleanup();
        soundMgr.cleanup();

        scene.cleanup();
        if (hudLoader != null) {
            hudLoader.cleanup();
        }
    }
    
    public HudLoader getHudLoader() {
    	return this.hudLoader;
    }
    
    public Scene getScene() {
    	return this.scene;
    }
    
    public Renderer getRenderer() {
    	return this.renderer;
    }
    
    public SoundManager getSoundManager() {
    	return this.soundMgr;
    }
}
