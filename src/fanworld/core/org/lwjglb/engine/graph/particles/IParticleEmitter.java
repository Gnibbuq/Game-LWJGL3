package fanworld.core.org.lwjglb.engine.graph.particles;

import java.util.List;

import fanworld.core.org.lwjglb.engine.items.GameItem;

public interface IParticleEmitter {

    void cleanup();
    
    Particle getBaseParticle();
    
    List<GameItem> getParticles();
}
