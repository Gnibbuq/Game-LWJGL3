package fanworld.core.org.lwjglb.engine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fanworld.core.org.lwjglb.engine.graph.InstancedMesh;
import fanworld.core.org.lwjglb.engine.graph.Mesh;
import fanworld.core.org.lwjglb.engine.graph.particles.IParticleEmitter;
import fanworld.core.org.lwjglb.engine.graph.weather.Fog;
import fanworld.core.org.lwjglb.engine.items.GameItem;
import fanworld.core.org.lwjglb.engine.items.SkyBox;
import fanworld.core.org.lwjglb.game.DummyGame;

public class Scene {

    private final Map<Mesh, List<GameItem>> meshMap;

    private final Map<InstancedMesh, List<GameItem>> instancedMeshMap;

    private SkyBox skyBox;

    private SceneLight sceneLight;

    private Fog fog;

    private boolean renderShadows;
    
    private IParticleEmitter[] particleEmitters;

    public Scene() {
        meshMap = new HashMap<>();
        instancedMeshMap = new HashMap<>();
        fog = Fog.NOFOG;
        renderShadows = true;
    }

    public Map<Mesh, List<GameItem>> getGameMeshes() {
        return meshMap;
    }

    public Map<InstancedMesh, List<GameItem>> getGameInstancedMeshes() {
        return instancedMeshMap;
    }
    
    public List<GameItem> getNeedRenderMeshes() {
        //List<GameItem> gameItems=new ArrayList<>();
    	DummyGame game=DummyGame.INSTANCE;
    	return game.getRenderer().getFilteredItems();
    	/**
        Vector3f pos=game.getCoreCamera().getPosition();
        World world=game.getGame().getGameClient().getCraftWorld();
        Location loc=LocationUtil.toLocation(world, pos);
        Chunk chunk=world.getChunk(loc);
        for (Block block : chunk.getAllBlock()) {//添加游戏内物品
        	//System.out.println("test:"+gameItem.isDisplay());
        	GameItem gameItem=block.getGameItem();
        	if(!gameItem.isDisplay())continue;
            if (gameItem.isInsideFrustum()) {
            	gameItems.add(gameItem);
            }
        }
        return gameItems;*/
    }

    public boolean isRenderShadows() {
        return renderShadows;
    }

    public void setGameItems(Collection<? extends GameItem> gameItems) {
        // Create a map of meshes to speed up rendering
        //int numGameItems = gameItems != null ? gameItems.size() : 0;
        for(GameItem gameItem:gameItems) {
            Mesh[] meshes = gameItem.getMeshes();
            if(meshes==null)continue;
            for (Mesh mesh : meshes) {
                boolean instancedMesh = mesh instanceof InstancedMesh;
                List<GameItem> list = instancedMesh ? instancedMeshMap.get(mesh) : meshMap.get(mesh);
                if (list == null) {
                    list = new ArrayList<>();
                    if (instancedMesh) {
                        instancedMeshMap.put((InstancedMesh)mesh, list);
                    } else {
                        meshMap.put(mesh, list);
                    }
                }
                list.add(gameItem);
            }
        }
    }

    public void addGameItem(GameItem gameItem) {
    	Mesh[] meshes = gameItem.getMeshes();
        for (Mesh mesh : meshes) {
            boolean instancedMesh = mesh instanceof InstancedMesh;
            List<GameItem> list = instancedMesh ? instancedMeshMap.get(mesh) : meshMap.get(mesh);
            if (list == null) {
                list = new ArrayList<>();
                if (instancedMesh) {
                    instancedMeshMap.put((InstancedMesh)mesh, list);
                } else {
                    meshMap.put(mesh, list);
                }
            }
            list.add(gameItem);
        }
    }
    
    public void cleanup() {
        for (Mesh mesh : meshMap.keySet()) {
            mesh.cleanUp();
        }
        for (Mesh mesh : instancedMeshMap.keySet()) {
            mesh.cleanUp();
        }
        if (particleEmitters != null) {
            for (IParticleEmitter particleEmitter : particleEmitters) {
                particleEmitter.cleanup();
            }
        }
    }

    public SkyBox getSkyBox() {
        return skyBox;
    }

    public void setRenderShadows(boolean renderShadows) {
        this.renderShadows = renderShadows;
    }

    public void setSkyBox(SkyBox skyBox) {
        this.skyBox = skyBox;
    }

    public SceneLight getSceneLight() {
        return sceneLight;
    }

    public void setSceneLight(SceneLight sceneLight) {
        this.sceneLight = sceneLight;
    }

    /**
     * @return the fog
     */
    public Fog getFog() {
        return fog;
    }

    /**
     * @param fog the fog to set
     */
    public void setFog(Fog fog) {
        this.fog = fog;
    }

    public IParticleEmitter[] getParticleEmitters() {
        return particleEmitters;
    }

    public void setParticleEmitters(IParticleEmitter[] particleEmitters) {
        this.particleEmitters = particleEmitters;
    }

}
