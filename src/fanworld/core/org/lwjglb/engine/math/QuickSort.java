package fanworld.core.org.lwjglb.engine.math;

import fanworld.game.utils.MathUtil;

/** 
* @author byxiaobai
* 类说明 
*/
@SuppressWarnings("unchecked")
public class QuickSort{
	static <T> void insertSort(Comparable<T> a[], int left, int right)
	{
	    for (int i = left + 1; i <= right; i++) {
	    	
	        for (int j = i; j > 0 && (a[j]!=null&&a[j].compareTo((T) a[j-1])==0) ; j--) {
	        	Comparable<T> temp=a[j];
	        	a[j]=a[j-1];
	        	a[j-1]=temp;
	        	
	        }
	    }
	}

	static <T> void swapArrayIndex(Comparable<T> a[],int index1,int index2) {
		Comparable<T> tmp = (Comparable<T>) a[index1];
	    a[index1] = a[index2];
	    a[index2] = tmp;
	}
	
	static <T> void quickSort(Comparable<T> a[], int left, int right)
	{
	    if (left >= right)
	        return;
	    if (right - left + 1 < 10)
	    {
	        //insertSort(a, left, right);
	        //return;
	    }
	    int i = left, j = right, k, flag = 0, pivot = MathUtil.getRandomNumber(1,100) % (right - left + 1) + left;
	    Comparable<T> tmp = a[left];
	    a[left] = a[pivot];
	    a[pivot] = tmp;
	    while (i < j)
	    {
	        while (j > i && (a[j]!=null&&(a[j].compareTo((T) a[left])==1)))//>=
	        {
	            if (a[j] == a[left])
	            {
	                for (k = j-1; k > i; k--)
	                    if (a[k] != a[j])
	                    {
	                        tmp = a[k];
	                        a[k] = a[j];
	                	    a[j] = tmp;
	                        break;
	                    }
	                if (k == i)
	                {
	                    if (a[left].compareTo((T) a[i])==1) {//>=
	                    	tmp = a[left];
	                        a[left] = a[i];
	                	    a[i] = tmp;
	                    }
	                        
	                    else
	                    {
	                    	swapArrayIndex(a,i,j);
	                        swapArrayIndex(a,left,i-1);
	                        i--;
	                        j--;
	                    }
	                    flag = 1;
	                    break;
	                }
	                else continue;
	            }
	            j--;
	        }
	        if (flag==1) break;
	        while (i < j && (a[i]!=null&&(a[i].compareTo((T) a[left])==0)))//<=
	        {
	            if (a[i] == a[left] && i != left)
	            {
	                for (k = i+1; k < j; k++)
	                {
	                    if (a[k] != a[i])
	                    {
	                    	swapArrayIndex(a,k,i);
	                        break;
	                    }
	                }
	                if (k == j)
	                {
	                	swapArrayIndex(a,left,j);
	                    flag = 1;
	                    break;
	                }
	                else continue;
	            }
	            i++;
	        }
	        if (flag==1) break;
	        swapArrayIndex(a,i,(i==j)?left:j);
	    }
	    quickSort(a, left, i-1);
	    quickSort(a, j+1, right);
	}

	static class ppoint implements Comparable<ppoint>{
		public int value;
		ppoint(int v){
			this.value=v;
		}
		@Override
		public int compareTo(ppoint o) {
			if(o==null)return 1;
			return this.value>=o.value?1:0;
		}
	}
	
	
	public static <T> void sort(Comparable<T>[] a,int size) {
		quickSort(a, 0, size);
	}
}
