package fanworld.core.org.lwjglb.engine.math;

/** 
* @author byxiaobai
*  凸包
*/
public class ConvexHull {
	public static class point implements Comparable<point>{
		double x,y;
		point(double x,double y){
			this.x=x;
			this.y=y;
		}
		@Override
		public int compareTo(point b) {
			if(b==null)return 0;
			return this.x<b.x||(this.x==b.x&&this.y<b.y)==true?1:0;
		}
	}
	public point[] p=new point[100010];
	public point getvec(point a,point b){
		if(a==null||b==null)return null;
		return new point(b.x-a.x,b.y-a.y);
	}
	public static double dis(point a,point b){
		if(a==null||b==null)return 0;
		return Math.sqrt((b.x-a.x)*(b.x-a.x)+(b.y-a.y)*(b.y-a.y));
	}
	public static double xmul(point a,point b) {//叉积
		if(a==null||b==null)return 0;
		return a.x*b.y-b.x*a.y;
	}
	public int n;
	public int[] stck=new int[100010];
	static int cntt=0;
	public void newpoint(double x,double y) {
		p[cntt]=new point(x,y);
		cntt++;
	}
	int cnt=0;
	public ConvexHull(int size,point...p) {
		this.p=p;
		n=size;
		//for(int i=1;i<=n;i++){
		//	p[i].x=Integer.parseInt(args[cnt]);
		//	p[i].y=Integer.parseInt(args[cnt]);
		//	cnt++;
		//}

		//Arrays.sort(p);
		QuickSort.sort(p, n);
		stck[++cnt]=1;
		stck[++cnt]=2;
		for(int i=3;i<=n;i++){
			point u=getvec(p[stck[cnt-1]],p[stck[cnt]]);
			point v=getvec(p[stck[cnt]],p[i]);
			while(xmul(u,v)<0.0){
				if(cnt==1)break;
				cnt--;
				u=getvec(p[stck[cnt-1]],p[stck[cnt]]);
				v=getvec(p[stck[cnt]],p[i]);
			}
			stck[++cnt]=i;
		}
		int tmp=cnt;
		stck[++cnt]=n;
		stck[++cnt]=n-1;
		for(int i=n-2;i>=1;i--){
			point u=getvec(p[stck[cnt-1]],p[stck[cnt]]);
			point v=getvec(p[stck[cnt]],p[i]);
			while(xmul(u,v)<0.0){
				if(cnt==tmp+1)break;
				cnt--;
				u=getvec(p[stck[cnt-1]],p[stck[cnt]]);
				v=getvec(p[stck[cnt]],p[i]);
			}
			stck[++cnt]=i;
		}
	}
	
	public point[] getHullPoints() {
		point[] pp=new point[cnt+1];
		for(int i=1;i<=cnt;i++) {
			pp[i]=p[stck[i]];
		}
		double ans=0;
		for(int i=1;i<=cnt-1;i++){
			ans+=dis(p[stck[i]],p[stck[i+1]]);
		}
		System.out.println(ans);
		return pp;
	}
	public static void main(String[] args) { 
		ConvexHull hull=new ConvexHull(3,new point(2,9),new point(6,3),new point(7,2),null);
		point[] pp=hull.getHullPoints();
		double ans=0;
		for(int i=1;i<=5-1;i++){
			ans+=dis(pp[i],pp[i+1]);
		}
		System.out.println(ans);
		return;
	}
}
