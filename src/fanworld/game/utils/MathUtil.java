package fanworld.game.utils;

import java.util.Random;

/** 
* @author byxiaobai
* 类说明 
*/
public class MathUtil {
	private static final Random RANDOM=new Random();
	/**
	 * 得到两个数之间的数组(包括这两个数)
	 * 输入要求为正整数
	 * @param num1 较小数
	 * @param num2 较大数
	 * @return
	 */
	public static int[] getNumberToNumber(int num1,int num2) {
		int numbersNumber=num2-num1+1;//数字数量
		int[] nums=new int[numbersNumber];
		for(int i=0;i<numbersNumber;i++,num1++) {
			nums[i]=num1;
		}
		return nums;
	}
	/**
	 * 查看某个数字是否在范围之内
	 * @param number 要判断的数字
	 * @param num1 范围中较小的数字
	 * @param num2 范围中较大的数字
	 * @return
	 */
	public static boolean isInRange(int number,int num1,int num2) {
		if(number>=num1&&number<=num2)
			return true;
		return false;
	}
	/**
	 * 得到一个随机数
	 * @param min
	 * @param max
	 * @return
	 */
	public static int getRandomNumber(int min,int max) {
		int randNumber =RANDOM.nextInt(max - min + 1) + min; // randNumber 将被赋值为一个 MIN 和 MAX 范围内的随机数
		return randNumber;
	}
	
	public static double getRandomNumber(double min,double max) {
		double randNumber =RANDOM.nextDouble()*(max - min + 1) + min; // randNumber 将被赋值为一个 MIN 和 MAX 范围内的随机数
		return randNumber;
	}
	
	public static double getBiggestNumber(double...numbers) {
		double max=-1;
		for(double number:numbers) {
			if(number>max)
				max=number;
		}
		return max;
	}
	public static double getSmallestNumber(double...numbers) {
		double max=-1;
		for(double number:numbers) {
			if(number<max)
				max=number;
		}
		return max;
	}
	public static double getBiggerNumber(double number1,double number2) {
		if(number1>number2) {
			return number1;
		}
		return number2;
	}
	public static double abs(double d) {
		if(d<0)return -d;
		return d;
	}
}