package fanworld.game.world;

import fanworld.game.world.block.Block;
import fanworld.game.world.block.blockdata.BlockType;
import fanworld.game.world.block.chunk.Chunk;
import fanworld.game.world.entity.Entity;

/** 
* @author byxiaobai
* 游戏中的一个世界
*/
public interface World {
	/**
	 * 获取世界名
	 * 注:每个存在的世界都必须有一个独立的世界名,不区分大小写
	 * @return
	 */
	public String getWorldName();
	/**
	 * 向世界中的某个位置添加一个实体
	 * @param entity 实体
	 * @param location 位置
	 */
	public void addEntity(Entity entity,Location location);
	/**
	 * 向世界中的某个位置添加一个方块
	 * @param blockTypeData
	 * @param location
	 */
	public void addBlock(Block block);
	/**
	 * 获取世界某个位置的方块
	 * @param location
	 * @return
	 */
	public Block getBlock(Location location);
	
	/**
	 * 设置坐标位置的方块类型
	 * @param loc
	 * @param block
	 */
	public void setBlock(Location loc, BlockType blockType);
	
	public WorldNoise getNoise();
	/**
	 * 获取坐标所在区块
	 * @param location
	 * @return
	 */
	public Chunk getChunk(Location location);
}
