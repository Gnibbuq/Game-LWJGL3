package fanworld.game.world;

/** 
* @author byxiaobai
* 默认的世界噪声实现
*/
public class CraftNoise implements WorldNoise{
	public CraftNoise() {
	}
	@Override
	public int getY(int x, int z) {
		double val=SimplexNoise.noise(((double)x)/64, ((double)z)/64)*10;
		//System.out.println("noise val:"+val);
		return Math.abs((int)val);
	}
	/**
	 * 散列化
	 * @param key
	 * @param prime
	 * @return
	 */
	@SuppressWarnings("unused")
	private static int additiveHash(String key, int prime) { 
		int hash, i; 
		for (hash = key.length(), i = 0; i < key.length(); i++)   
			hash += key.charAt(i); 
		return (hash % prime); 
	}
}
