package fanworld.game.world.block.blockdata;

import fanworld.core.org.lwjglb.engine.graph.Texture;

/** 
* @author byxiaobai
* 类说明 
*/
public class Grass extends CraftBlockTypeData{
	private static final float REFLECTANCE=0.1f;
	public Grass() {
		super();
		try {
			Texture texture;
			texture = new Texture("/textures/terrain_textures.png", 2, 1);
			init(texture,REFLECTANCE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
