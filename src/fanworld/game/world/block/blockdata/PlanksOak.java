package fanworld.game.world.block.blockdata;

import fanworld.core.org.lwjglb.engine.graph.Texture;

/** 
* @author byxiaobai
* 木板
*/
public class PlanksOak extends CraftBlockTypeData{
	private static final float REFLECTANCE=0.1f;
	public PlanksOak() throws Exception {
		super();
		Texture texture = new Texture("/textures/planks_oak.png", 2, 1);
		init(texture,REFLECTANCE);
	}

}
