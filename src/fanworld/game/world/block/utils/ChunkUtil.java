package fanworld.game.world.block.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import fanworld.game.utils.MathUtil;
import fanworld.game.world.Location;
import fanworld.game.world.World;
import fanworld.game.world.block.Block;
import fanworld.game.world.block.chunk.Chunk;

/** 
* @author byxiaobai
* 类说明 
*/
public class ChunkUtil {
	
	/**
	 * 获取周围的区块
	 * @param location
	 * @param range
	 * @return
	 */
	public static List<Chunk> getNearByChunks(Location location,double range){
		World world=location.getWorld();
		location=LocationUtil.toBlockLocation(location);
		int x=(int)location.getX();
		int z=(int)location.getZ();
		List<Chunk> chunks=new ArrayList<>();
		//Location tmpLoc=new Location(world,-150,6,141);
		for(int i=-32;i<=range*16;i+=16) {
			for(int j=-32;j<=range*16;j+=16) {
				Location tmpLoc=new Location(world,x+i,1,z+j);
				chunks.add(world.getChunk(tmpLoc));
			}
		}
		return chunks;
	}
	
	/**
	 * 获取顶层随机一个方块的坐标
	 * @param chunk
	 * @return
	 */
	public static Location getRandomFrontBlockLocation(Chunk chunk) {
		Collection<? extends Block> blocks=chunk.getFrontBlock();
		int size=blocks.size();
		int randomValue=MathUtil.getRandomNumber(1, size);
		int cnt=0;
		Block nowBlock=null;
		for(Block block:blocks) {
			cnt++;
			nowBlock=block;
			if(cnt==randomValue)break;
		}
		return nowBlock.getLocation();
	}
	
}
