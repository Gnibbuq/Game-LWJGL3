package fanworld.game.world.block.tree;

import fanworld.core.org.lwjglb.engine.graph.Texture;
import fanworld.game.world.block.blockdata.CraftBlockTypeData;

/** 
* @author byxiaobai
* 白烨木
*/
public class BirchLog extends CraftBlockTypeData{
	private static final float REFLECTANCE=0.1f;
	public BirchLog() {
		super();
		try {
			Texture texture= new Texture("/textures/trees/birch_log.png", 2, 1);
			init(texture,REFLECTANCE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
