package fanworld.game.world.block.chunk;

import java.util.Collection;

import fanworld.game.world.Location;
import fanworld.game.world.block.Block;

/** 
* @author byxiaobai
* 世界单位元的组成结构，介于世界与单位元之间
*/
public interface Chunk {
	/**
	 * 获取区块中的所有顶层方块
	 * @return
	 */
	public Collection<? extends Block> getFrontBlock();
	public Collection<? extends Block> getAllBlocks();
	/**
	 * 添加方块
	 * @param block
	 */
	public void addBlock(Block block);
	public Block getBlock(Location location);
	public void init();
	public Block getCenterBlock();
}
