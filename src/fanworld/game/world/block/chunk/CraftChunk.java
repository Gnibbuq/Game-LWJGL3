package fanworld.game.world.block.chunk;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import fanworld.game.utils.MathUtil;
import fanworld.game.world.Location;
import fanworld.game.world.World;
import fanworld.game.world.block.Block;
import fanworld.game.world.block.tree.utils.TreeUtil;
import fanworld.game.world.block.utils.ChunkUtil;
import fanworld.game.world.block.utils.LocationUtil;

/** 
* @author byxiaobai
* 类说明 
*/
public class CraftChunk implements Chunk{
	private HashMap<Location,Block> blocks=new HashMap<>();
	private Location centerPoint;
	private World world;
	private HashMap<Integer,HashMap<Integer,Integer>> top=new HashMap<>();
	
	public CraftChunk(Location centerPoint) {
		//centerPoint=new Location(world,(minX+maxX)/2,0,(minZ+maxZ)/2);
		this.centerPoint=LocationUtil.toBlockLocation(centerPoint);
		this.world=centerPoint.getWorld();
	}

	@Override
	public Collection<Block> getFrontBlock() {
		List<Block> ans=new ArrayList<>();
		for(Block block:blocks.values()) {
			double x,y,z;
			Location loc=toInsideLocation(block.getLocation());
			x=loc.getX();y=loc.getY();z=loc.getZ();
			if(y==getTopY(x,z)) {
				ans.add(block);
			}
		}
		return ans;
	}
	
	public int getTopY(int x,int z) {
		if(top.get(x)==null) {
			top.put(x, new HashMap<>());
		}
		Integer ans=top.get(x).get(z);
		return ans!=null?ans:-1;
	}
	
	public int getTopY(double x,double z) {
		return getTopY((int)x,(int)z);
	}
	
	public int getTopY(Location loc) {
		loc=toInsideLocation(loc);
		return getTopY(loc.getX(),loc.getZ());
	}
	
	private void setTopY(Location loc,double value) {
		loc=toInsideLocation(loc);
		int x=(int)loc.getX();int z=(int)loc.getZ();
		top.get(x).put(z, (int)value);
	}
	
	private Location toInsideLocation(Location loc) {
		//cout.p("原始:"+loc.getX()+",now:"+(loc.getX()/16-centerPoint.getX()));
		return new Location(loc.getWorld(),loc.getX()-centerPoint.getX()+200,loc.getY()-centerPoint.getY(),loc.getZ()-centerPoint.getZ()+200);
	}

	@Override
	public void addBlock(Block block) {
		if(block==null)return;
		Location loc=LocationUtil.toBlockLocation(block.getLocation());
		blocks.put(loc, block);
		if(loc.getY()>getTopY(loc)) {
			setTopY(loc,loc.getY());
		}
	}

	@Override
	public Block getBlock(Location location) {
		return blocks.get(LocationUtil.toBlockLocation(location));
	}
	
	private boolean isInited=false;
	/**
	 * 初始化区块
	 */
	@Override
	public void init() {
		if(isInited)return;
		isInited=true;
		int randomValue=MathUtil.getRandomNumber(1, 100);
		if(randomValue<10) {
			Location frontLocation=ChunkUtil.getRandomFrontBlockLocation(this);
			frontLocation.setY(frontLocation.getY()+1);
			System.out.println(frontLocation);
			TreeUtil.createTree(frontLocation);
		}
	}

	@Override
	public Block getCenterBlock() {
		//if(this.centerBlock==null)init();
		//return this.centerBlock;
		for(Block block:this.blocks.values()) {
			return block;
		}
		return null;
	}

	public World getWorld() {
		return world;
	}

	@Override
	public Collection<? extends Block> getAllBlocks() {
		return blocks.values();
	}

}
