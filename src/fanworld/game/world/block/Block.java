package fanworld.game.world.block;

import fanworld.core.org.lwjglb.engine.items.GameItem;
import fanworld.game.world.Location;
import fanworld.game.world.block.blockdata.BlockType;

/** 
* @author byxiaobai
* 方块
*/
public interface Block {
	/**
	 * 获取方块坐标
	 * @return
	 */
	public Location getLocation();
	/**
	 * 获取方块类型
	 * @return
	 */
	public BlockType getBlockType();
	public GameItem getGameItem();
}
