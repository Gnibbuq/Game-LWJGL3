package fanworld.game.world.entity;

import fanworld.game.world.Location;

/** 
* @author byxiaobai
* 类说明 
*/
public class Player extends Human{
	private Location location;
	public Player(Location location) {
		this.location=location;
	}
	
	public Location getLocation() {
		return this.location;
	}
}
