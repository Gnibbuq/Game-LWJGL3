package fanworld.game.world.entity;
/** 
* @author byxiaobai
* 可以与世界交互的物体
*/
public interface Entity {
	public void create();
	public void remove();
}
