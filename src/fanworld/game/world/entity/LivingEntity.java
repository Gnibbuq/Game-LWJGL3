package fanworld.game.world.entity;

import fanworld.game.world.Location;
import fanworld.game.world.entity.ai.EntityAI;

/** 
* @author byxiaobai
* 有生命的独立实体
*/
public interface LivingEntity {
	public double getMaxHealth();
	public double getHealth();
	public Location getLocation();
	public void setLocation(Location location);
	/**
	 * 设置实体的行动方式
	 * @param entityAI
	 */
	public void setAI(EntityAI entityAI);
	public EntityAI getAI();
}
