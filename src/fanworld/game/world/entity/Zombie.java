package fanworld.game.world.entity;

import fanworld.game.world.Location;
import fanworld.game.world.entity.ai.EntityAI;

/** 
* @author byxiaobai
* 僵尸
*/
public class Zombie implements LivingEntity{
	private double maxHealth;
	private double health;
	private Location location;
	@Override
	public double getMaxHealth() {
		return maxHealth;
	}

	@Override
	public double getHealth() {
		return health;
	}

	@Override
	public Location getLocation() {
		return location.clone();
	}

	@Override
	public void setLocation(Location location) {
		this.location=location.clone();
	}

	@Override
	public void setAI(EntityAI entityAI) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public EntityAI getAI() {
		// TODO Auto-generated method stub
		return null;
	}

}
