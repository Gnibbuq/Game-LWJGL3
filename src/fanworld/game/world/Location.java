package fanworld.game.world;

import fanworld.game.world.block.chunk.Chunk;

/** 
* @author byxiaobai
* 世界中的一个位置 
*/
public class Location {
	private World world;
	private double locationX,locationY,locationZ;
	
	public Location(World world,double locationX,double locationY,double locationZ) {
		this.locationX=locationX;
		this.locationY=locationY;
		this.locationZ=locationZ;
		this.world=world;
	}
	
	/**
	 * 获取位置的x坐标
	 * @return
	 */
	public double getX() {
		return locationX;
	}
	/**
	 * 设置位置的x坐标
	 * @param locationX
	 */
	public void setX(double locationX) {
		this.locationX = locationX;
	}
	/**
	 * 获取位置的y坐标
	 * @return
	 */
	public double getY() {
		return locationY;
	}
	/**
	 * 设置位置的y坐标
	 * @param locationY
	 */
	public void setY(double locationY) {
		this.locationY = locationY;
	}
	/**
	 * 获取位置的z坐标
	 * @return
	 */
	public double getZ() {
		return locationZ;
	}
	/**
	 * 设置位置的z坐标
	 * @param locationZ
	 */
	public void setZ(double locationZ) {
		this.locationZ = locationZ;
	}
	public World getWorld() {
		return this.world;
	}
	public Chunk getChunk() {
		return world.getChunk(this);
	}
	public void teleport(Location location) {
		return;
	}
	
	@Override
	public String toString() {
		return "X:"+this.locationX+",Y:"+this.locationY+",Z:"+this.locationZ;
	}
	
	@Override
	public Location clone() {
		return new Location(this.world,this.locationX,this.locationY,this.locationZ);
	}
	@Override
	public boolean equals(Object another) {
		if(another instanceof Location) {
			Location loc=(Location)another;
			return this.locationX==loc.locationX&&this.locationY==loc.locationY&&this.locationZ==loc.locationZ;
		}
		return false;
	}
	@Override
	public int hashCode() {
		return (Double.toString(locationX)+Double.toString(locationY)+Double.toString(locationZ)).hashCode();
	}
}
