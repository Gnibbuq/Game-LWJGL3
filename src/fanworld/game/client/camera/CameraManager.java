package fanworld.game.client.camera;

import fanworld.core.org.lwjglb.engine.graph.CoreCamera;

/** 
* @author byxiaobai
* 类说明 
*/
public class CameraManager {
	private Camera camera;
	public CameraManager(CoreCamera coreCamera) {
		camera=new Camera(coreCamera);
	}
	public Camera getCamera() {
		return this.camera;
	}
}
