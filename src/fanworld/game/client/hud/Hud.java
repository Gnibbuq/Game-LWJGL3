package fanworld.game.client.hud;
/** 
* @author byxiaobai
* 类说明 
*/
public interface Hud {
	public void setDisplay(boolean isDisplay);
	/**
	 * 是否显示
	 * @return
	 */
	public boolean isDisplay();
	public void update();
}
