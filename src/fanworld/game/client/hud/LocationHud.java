package fanworld.game.client.hud;

import org.joml.Vector3f;

import fanworld.core.org.lwjglb.game.DummyGame;

/** 
* @author byxiaobai
* 类说明 
*/
public class LocationHud implements TextHud{
	private String text="TEST";
	
	@Override
	public void setDisplay(boolean isDisplay) {}

	@Override
	public boolean isDisplay() {
		return true;
	}

	public float getSize() {
		return 32.0f;
	}
	
	public float getX() {
		return 1275;
	}
	
	public float getY() {
		return 725;
	}
	
	@Override
	public void update() {
		Vector3f pos=DummyGame.INSTANCE.getCoreCamera().getPosition();
		this.text="X: "+(int)pos.x+",Y: "+(int)pos.y+",Z: "+(int)pos.z;
	}

	@Override
	public void setText(String text) {
		this.text=text;
	}

	@Override
	public String getText() {
		return text;
	}

}
