package fanworld.game.client.input.keyboard;

import java.util.HashMap;

import org.lwjgl.glfw.GLFW;

import fanworld.core.org.lwjglb.engine.Window;

/** 
* @author byxiaobai
* 类说明 
*/
public class KeyBoardUtil {
	private static Window window;
	private static HashMap<Integer,String> charMap;
	static {
		charMap=new HashMap<>();
		charMap.put(GLFW.GLFW_KEY_A, "a");
		charMap.put(GLFW.GLFW_KEY_B, "b");
		charMap.put(GLFW.GLFW_KEY_C, "c");
		charMap.put(GLFW.GLFW_KEY_D, "d");
		charMap.put(GLFW.GLFW_KEY_E, "e");
		charMap.put(GLFW.GLFW_KEY_F, "f");
		charMap.put(GLFW.GLFW_KEY_G, "g");
		charMap.put(GLFW.GLFW_KEY_H, "h");
		charMap.put(GLFW.GLFW_KEY_I, "i");
		charMap.put(GLFW.GLFW_KEY_J, "j");
		charMap.put(GLFW.GLFW_KEY_K, "k");
		charMap.put(GLFW.GLFW_KEY_L, "l");
		charMap.put(GLFW.GLFW_KEY_M, "m");
		charMap.put(GLFW.GLFW_KEY_N, "n");
		charMap.put(GLFW.GLFW_KEY_O, "o");
		charMap.put(GLFW.GLFW_KEY_P, "p");
		charMap.put(GLFW.GLFW_KEY_Q, "q");
		charMap.put(GLFW.GLFW_KEY_R, "r");
		charMap.put(GLFW.GLFW_KEY_S, "s");
		charMap.put(GLFW.GLFW_KEY_T, "t");
		charMap.put(GLFW.GLFW_KEY_U, "u");
		charMap.put(GLFW.GLFW_KEY_V, "v");
		charMap.put(GLFW.GLFW_KEY_W, "w");
		charMap.put(GLFW.GLFW_KEY_X, "x");
		charMap.put(GLFW.GLFW_KEY_Y, "y");
		charMap.put(GLFW.GLFW_KEY_Z, "z");
		charMap.put(GLFW.GLFW_KEY_1, "1");
		charMap.put(GLFW.GLFW_KEY_2, "2");
		charMap.put(GLFW.GLFW_KEY_3, "3");
		charMap.put(GLFW.GLFW_KEY_4, "4");
		charMap.put(GLFW.GLFW_KEY_5, "5");
		charMap.put(GLFW.GLFW_KEY_6, "6");
		charMap.put(GLFW.GLFW_KEY_7, "7");
		charMap.put(GLFW.GLFW_KEY_8, "8");
		charMap.put(GLFW.GLFW_KEY_9, "9");
		charMap.put(GLFW.GLFW_KEY_0, "0");
		charMap.put(GLFW.GLFW_KEY_SPACE, " ");
		charMap.put(GLFW.GLFW_KEY_BACKSPACE, "BACK");
	}
	
	private static HashMap<Integer,Long> oldPressMap=new HashMap<>();
	
	private static boolean isOldPressed(int key) {
		Long oldTime=oldPressMap.get(key);
		if(oldTime==null)return false;
		if(System.currentTimeMillis()-oldTime<27)return true;
		return false;
	}
	
	public static String getNowInputChar() {
		for(int key:charMap.keySet()) {
			if(window.isKeyPressed(key)) {
				if(!isOldPressed(key)) {
					oldPressMap.put(key, System.currentTimeMillis());
					return charMap.get(key);
				}
				oldPressMap.put(key, System.currentTimeMillis());
			}
		}
		return "";
	}
	
	public static void setWindow(Window window) {
		KeyBoardUtil.window=window;
	}
}
