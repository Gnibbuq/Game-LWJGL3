package fanworld.game.client.command;
/** 
* @author byxiaobai
* 指令监听器
*/
public interface CommandInstance {
	public void run(String[] args);
}
