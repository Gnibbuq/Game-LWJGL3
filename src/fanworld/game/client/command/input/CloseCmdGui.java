package fanworld.game.client.command.input;

import fanworld.game.client.GameData;
import fanworld.game.client.command.CommandHud;
import fanworld.game.client.command.CommandManager;
import fanworld.game.client.hud.HudManager;
import fanworld.game.client.input.keyboard.KeyBoard;
import fanworld.game.client.input.keyboard.KeyBoardManager;

/** 
* @author byxiaobai
* 关闭指令窗口
*/
public class CloseCmdGui extends KeyBoard{
	public void init(int key) {
		KeyBoardManager.INSTANCE.registerKey(key, this);
	}

	@Override
	public void handle() {
		if(!GameData.INSTANCE.isCmdGuiOpen||System.currentTimeMillis()-GameData.INSTANCE.cmdGuiOpenTime<25)return;
		CommandHud cmdHud=(CommandHud)HudManager.INSTANCE.getHud("Cmd");
		CommandManager.INSTANCE.run(cmdHud.getText());
		cmdHud.setText("");
		CommandManager.INSTANCE.setUsingCommand(false);
		GameData.INSTANCE.isCmdGuiOpen=false;
	}
}
