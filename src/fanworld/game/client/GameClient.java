package fanworld.game.client;

import fanworld.core.org.lwjglb.engine.graph.CoreCamera;
import fanworld.game.client.camera.CameraManager;
import fanworld.game.client.hud.HudManager;
import fanworld.game.client.input.keyboard.KeyBoardManager;
import fanworld.game.world.CraftWorld;

/** 
* @author byxiaobai
* 类说明 
*/
public class GameClient {
	private CameraManager cameraManager;
	private CraftWorld craftWorld;
	/**
	 * HUD管理器
	 */
	private HudManager hudManager;
	public GameClient() {
		craftWorld=new CraftWorld();
	}
	public GameClient(CoreCamera coreCamera) {
		this();
		setCameraManager(new CameraManager(coreCamera));
		hudManager=HudManager.INSTANCE;
		hudManager.init();
		KeyBoardManager.INSTANCE.init();
	}
	public CraftWorld getCraftWorld() {
		return this.craftWorld;
	}
	public HudManager getHudManager() {
		return hudManager;
	}
	public void setHudManager(HudManager hudManager) {
		this.hudManager = hudManager;
	}
	public CameraManager getCameraManager() {
		return cameraManager;
	}
	public void setCameraManager(CameraManager cameraManager) {
		this.cameraManager = cameraManager;
	}
}
