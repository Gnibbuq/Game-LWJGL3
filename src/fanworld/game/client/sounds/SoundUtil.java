package fanworld.game.client.sounds;

import java.util.HashMap;

import org.joml.Vector3f;

import fanworld.core.org.lwjglb.engine.sound.SoundManager;
import fanworld.core.org.lwjglb.engine.sound.SoundSource;
import fanworld.core.org.lwjglb.game.DummyGame;
import fanworld.core.org.lwjglb.game.SoundBuffer;
import fanworld.game.utils.MathUtil;

public class SoundUtil {
	private static HashMap<Integer,String> grassSoundMap=new HashMap<>();
	
	static {
		grassSoundMap.put(1, "grass1");
		grassSoundMap.put(2, "grass2");
		grassSoundMap.put(3, "wet_grass1");
		grassSoundMap.put(4, "wet_grass2");
		grassSoundMap.put(5, "wet_grass3");
	}
	
	public static void playSound(Vector3f vector,String soundName) {
		SoundManager soundMgr=DummyGame.INSTANCE.getSoundManager();
		SoundBuffer buffFire;
		try {
			buffFire = new SoundBuffer("/sounds/"+soundName+".ogg");
	        soundMgr.addSoundBuffer(buffFire);
	        SoundSource sourceFire = new SoundSource(false,false);
	        Vector3f pos = vector;
	        sourceFire.setPosition(pos);
	        sourceFire.setBuffer(buffFire.getBufferId());
	        soundMgr.addSoundSource(soundName, sourceFire);
	        sourceFire.setGain(1.0f);
	        sourceFire.play();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void playGrassSound(Vector3f position) {
		int num=MathUtil.getRandomNumber(1, 5);
		String name=grassSoundMap.get(num);
		playSound(position,name);
	}
}
